#include<stdio.h>
#include<pthread.h>
#include<inttypes.h>

void count_to_10(void * arg) {
    for (uint8_t i = 0; i < 20; ++i) {
        printf("%" PRIu8 "\n", i);
    }
}


int main(int argc, char ** argv) {
    pthread_t threads[5];
    count_to_10(NULL);
    for (uint8_t i=0; i < 5; ++i) {
        pthread_create(&threads[i], NULL, count_to_10, NULL);
    }
}

