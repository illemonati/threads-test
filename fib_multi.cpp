#include<iostream>
#include<inttypes.h>
#include<thread>
#include<future>
#include<vector>

using namespace std;

uint64_t fib(uint64_t n) {
    uint64_t a = 0;
    uint64_t b = 1;
    for (uint64_t i = 0; i < n; ++i) {
        uint64_t temp = a;
        a = b;
        b += temp;
    }
    return a;
}

int main() {
    vector<future<uint64_t>> fibs;
    for (uint64_t i = 0; i < 30; ++i) {
        fibs.emplace_back(async(fib, i));
    }

    for (auto &n : fibs) {
        cout << n.get() << " ";
    }

    cout << endl;

    return 0;

}


