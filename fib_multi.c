#include<threads.h>
#include<stdio.h>
#include<inttypes.h>
#define len(a) sizeof(a)/sizeof(a[0])
#define UNTIL 30


typedef struct FibArgs {
    uint64_t n;
    uint64_t res;
} FibArgs;


uint64_t fib(uint64_t n) {
    uint64_t a = 0;
    uint64_t b = 1;
    for (uint64_t i = 0; i < n; ++i) {
        uint64_t temp = a;
        a = b;
        b += temp;
    }
    return a;
}



int fib_thread(void *args) {

    FibArgs *fargs = (FibArgs *)args;

    fargs->res = fib(fargs->n);

    thrd_exit(0);
}

int main() {

    thrd_t tids[UNTIL];
    FibArgs args[UNTIL];

    for (uint8_t i = 0; i < UNTIL; ++i) {
        args[i].n = i;
        thrd_create(&tids[i], fib_thread, &args[i]);
    }

    for (uint8_t i = 0; i < UNTIL; ++i) {
        thrd_join(tids[i], NULL);
        printf("%" PRIu64 " ", args[i].res);
    }
    printf("\n");



    return 0;
}
